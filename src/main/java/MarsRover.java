import java.util.Objects;

public class MarsRover {
    private final Position position;
    private final Direction direction;

    MarsRover(Position position, Direction direction) {
        this.position = position;
        this.direction = direction;
    }

    public MarsRover move() {
        Position newPosition = null;
        if (direction == Direction.N){
            newPosition = new Position(position.getX(), position.getY() + 1);
        }
        if (direction == Direction.E){
            newPosition = new Position(position.getX() + 1 , position.getY());
        }
        if (direction == Direction.S){
            newPosition = new Position(position.getX() , position.getY() - 1);
        }
        if (direction == Direction.W){
            newPosition = new Position(position.getX() - 1 , position.getY());
        }

        return new MarsRover(newPosition, direction);
    }

    public MarsRover turnLeft() {
        return new MarsRover(position, direction.turnLeft());
    }

    public MarsRover turnRight() {
        return new MarsRover(position, direction.turnRight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarsRover marsRover = (MarsRover) o;
        return position.equals(marsRover.position) && direction == marsRover.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, direction);
    }

    @Override
    public String toString() {
        return "MarsRover{" +
                position +
                direction +
                '}';
    }


    public MarsRover execute(Command command) {
        switch (command) {
            case M:
                return this.move();
            case L:
                return this.turnLeft();
            case R:
                return this.turnRight();
        }
        return null;
    }

    public static MarsRover executeCommands(MarsRover initialRover, String commands) {
        return commands.chars()
                .mapToObj(i -> String.valueOf((char) i))
                .map(Command::valueOf)
                .reduce(initialRover, MarsRover::execute, (a, b) -> b);
    }
}
