import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandsTest {
    @Test
    void return_Rover_neg1_1_N_when_execute_MLMR_given_Rover_0_0_N(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.N);

        //when
        MarsRover actualMarsRover = marsRover.executeCommands(marsRover, "MLMR");

        //then
        MarsRover expectedMarsRover = new MarsRover(new Position(-1, 1), Direction.N);
        assertEquals(expectedMarsRover,actualMarsRover);
    }
}
