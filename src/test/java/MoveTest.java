import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveTest {
    @Test
    void return_rover_0_0_N_when_move_given_rover_0_0_N() {
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.N);

        //when
        MarsRover actualNewRover = marsRover.move();
        //then
        MarsRover expectedNewRover = new MarsRover(new Position(0, 1), Direction.N);
        assertEquals(expectedNewRover, actualNewRover);
    }
    @Test
    void return_rover_0_0_E_when_move_given_rover_1_0_E() {
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.E);

        //when
        MarsRover actualNewRover = marsRover.move();
        //then
        MarsRover expectedNewRover = new MarsRover(new Position(1, 0), Direction.E);
        assertEquals(expectedNewRover, actualNewRover);
    }
    @Test
    void return_rover_0_0_S_when_move_given_rover_0_neg0_S() {
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.S);

        //when
        MarsRover actualNewRover = marsRover.move();
        //then
        MarsRover expectedNewRover = new MarsRover(new Position(0, -1), Direction.S);
        assertEquals(expectedNewRover, actualNewRover);
    }

    @Test
    void return_rover_0_0_S_when_move_given_rover_neg0_0_W() {
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.W);

        //when
        MarsRover actualNewRover = marsRover.move();
        //then
        MarsRover expectedNewRover = new MarsRover(new Position(-1, 0), Direction.W);
        assertEquals(expectedNewRover, actualNewRover);
    }
}
