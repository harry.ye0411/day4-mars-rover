import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandTest {
    @Test
    void return_Rover_0_1_N_when_execute_M_given_0_0_N(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.N);
        //when
        MarsRover actualNewRover = marsRover.execute(Command.M);
        //then
        MarsRover expectedNewRover = new MarsRover(new Position(0, 1), Direction.N);
        assertEquals(expectedNewRover, actualNewRover);
    }

    @Test
    void return_Rover_0_0_W_when_execute_L_given_0_0_N(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.N);
        //when
        MarsRover actualNewRover = marsRover.execute(Command.L);
        //then
        MarsRover expectedNewRover = new MarsRover(new Position(0, 0), Direction.W);
        assertEquals(expectedNewRover, actualNewRover);
    }

    @Test
    void return_Rover_0_0_E_when_execute_R_given_0_0_N(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.N);
        //when
        MarsRover actualNewRover = marsRover.execute(Command.R);
        //then
        MarsRover expectedNewRover = new MarsRover(new Position(0, 0), Direction.E);
        assertEquals(expectedNewRover, actualNewRover);
    }
}
