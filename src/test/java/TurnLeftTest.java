import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TurnLeftTest {
    @Test
    void return_Rover_W_when_TurnLeft_given_Rover_N(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.N);

        //when
        MarsRover actualMarsRover = marsRover.turnLeft();

        //then
        MarsRover expectedMarsRover = new MarsRover(initPosition, Direction.W);
        assertEquals(expectedMarsRover,actualMarsRover);
    }
    @Test
    void return_Rover_N_when_TurnLeft_given_Rover_E(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.E);

        //when
        MarsRover actualMarsRover = marsRover.turnLeft();

        //then
        MarsRover expectedMarsRover = new MarsRover(initPosition, Direction.N);
        assertEquals(expectedMarsRover,actualMarsRover);
    }
    @Test
    void return_Rover_E_when_TurnLeft_given_Rover_S(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.S);

        //when
        MarsRover actualMarsRover = marsRover.turnLeft();

        //then
        MarsRover expectedMarsRover = new MarsRover(initPosition, Direction.E);
        assertEquals(expectedMarsRover,actualMarsRover);
    }
    @Test
    void return_Rover_S_when_TurnLeft_given_Rover_W(){
        //given
        Position initPosition = new Position(0, 0);
        MarsRover marsRover = new MarsRover(initPosition, Direction.W);

        //when
        MarsRover actualMarsRover = marsRover.turnLeft();

        //then
        MarsRover expectedMarsRover = new MarsRover(initPosition, Direction.S);
        assertEquals(expectedMarsRover,actualMarsRover);
    }
}
